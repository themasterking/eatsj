import 'package:flutter/material.dart';

class RoundedInput extends StatelessWidget {
  final String placeholder;
  final IconData icon;
  final bool secured;
  final TextInputType inputType;
  final TextEditingController controller;
  final ValueChanged<String> onChange;

  // const RoundedInput({ Key: key }):super(key: key)
  RoundedInput(
      {Key key,
      @required this.placeholder,
      this.icon,
      this.secured = false,
      this.onChange,
      this.inputType = TextInputType.text,
      this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      // padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * .8,
      decoration: BoxDecoration(
        color: Color.fromRGBO(255, 255, 255, .32),
        borderRadius: BorderRadius.circular(29),
        border: Border.all(color: Color.fromRGBO(255, 255, 255, .5)),
      ),
      child: Row(
        children: [
          Container(
            height: 50,
            width: 50,
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColorDark,
                borderRadius: BorderRadius.circular(50)),
            child: this.icon != null
                ? Icon(icon, color: Theme.of(context).primaryColor)
                : null,
          ),
          SizedBox(
            width: 10,
          ),
          Flexible(
            child: TextField(
              controller: controller,
              maxLines: 1,
              enableSuggestions: false,
              autocorrect: false,
              obscureText: this.secured,
              keyboardType: this.inputType,
              style: TextStyle(
                color: Theme.of(context).primaryColorDark,
                fontFamily: 'Montserrat',
              ),
              decoration: InputDecoration(
                  hintText: placeholder,
                  hintStyle: TextStyle(
                    color: Theme.of(context).primaryColorDark,
                    fontFamily: 'Montserrat',
                  ),
                  filled: true,
                  border: InputBorder.none),
            ),
          )
          // TextField()
        ],
      ),
    );
  }
}
