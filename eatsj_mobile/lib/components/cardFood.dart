import 'package:flutter/material.dart';

class CardFood extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(10),
          height: 180,
          width: 160,
          decoration: BoxDecoration(
            color: Colors.blueAccent,
            borderRadius: BorderRadius.circular(16),
          ),
          child: Image.asset('assets/images/default.jpg'),
        ),
        Text(
          'Eru',
          style: TextStyle(color: Colors.black),
        ),
      ],
    );
  }
}
