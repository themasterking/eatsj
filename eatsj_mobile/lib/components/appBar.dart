import 'package:flutter/material.dart';

AppBar buildAppBar(context) {
  return AppBar(
    backgroundColor: Theme.of(context).primaryColor,
    elevation: 0,
    actions: <Widget>[
      IconButton(
        icon: Icon(Icons.search, color: Theme.of(context).primaryColorDark),
        onPressed: null,
      ),
      IconButton(
        icon: Icon(Icons.shopping_cart,
            color: Theme.of(context).primaryColorDark),
        onPressed: null,
      ),
    ],
  );
}
