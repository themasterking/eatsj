import 'package:flutter/material.dart';
import 'utils/themes.dart';
import 'utils/routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            primaryColor: primaryColor,
            accentColor: secondaryColor,
            primaryColorDark: backgroundColor,
            fontFamily: defaultFont,
            textTheme: TextTheme(
                headline1: TextStyle(
                    color: defaultTextColor,
                    fontSize: 72.0,
                    fontFamily: titleFont,
                    fontWeight: FontWeight.bold),
                // headline2: TextStyle(color: Colors.white, fontSize: 72.0)
                bodyText1: TextStyle(color: defaultTextColor, fontSize: 72.0),
                headline4: TextStyle(
                    color: predefaultTextColor,
                    fontSize: 68.0,
                    fontFamily: titleFont,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic),
                headline6: TextStyle(
                    color: predefaultTextColor,
                    fontSize: 14.0,
                    fontFamily: titleFont),
                bodyText2:
                    TextStyle(color: predefaultTextColor, fontSize: 18.0)),
            backgroundColor: backgroundColor),
        initialRoute: '/splash',
        routes: routes);
  }
}
