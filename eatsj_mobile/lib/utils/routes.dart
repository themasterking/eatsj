import '../screens/splashScreen.dart';
import '../screens/loginScreen.dart';
import '../screens/homeScreen.dart';

final routes = {
  '/': (context) => new HomeScreen(),
  '/splash': (context) => new SplashScreen(),
  '/login': (context) => new LoginScreen(),
};
