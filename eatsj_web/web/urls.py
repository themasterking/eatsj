from django.urls import path, include

from . import views


app_name = "eatsj"


urlpatterns = [
    path('', views.HomeView.as_view(), name="home"),
    path('leaderboard', views.LeaderboardView.as_view(), name="leaderboard"),
    path('histories', views.HistoriesListView.as_view(), name="history_list"),

    path('auth/', include([
        path('account/', views.AccountView.as_view(), name='account'),
        path('signin/', views.SigninView.as_view(), name="signin"),
        path('signout/', views.signout, name="signout"),
    ])),

    path('users/', include([
        path('', views.UserListView.as_view(), name="user_list"),
        path('save/', views.UserCreateView.as_view(), name="user_create"),
        path('<pk>/details/', views.UserDetailsView.as_view(), name="user_details"),
        path('<pk>/delete/', views.user_delete_view, name="user_delete"),
    ])),

    path('products/', include([
        path('menus/', include([
            path('', views.ProductsMenuListView.as_view(), name="products_menu_list"),
            path('save/', views.ProductsMenuCreateView.as_view(), name="products_menu_create"),
        ])),
        path('foods/', include([
            path('', views.ProductsFoodListView.as_view(), name="products_food_list"),
            path('save/', views.ProductsFoodCreateView.as_view(), name="products_food_create"),
            path('<pk>/details/', views.ProductsFoodDetailsView.as_view(), name="products_food_details"),
            path('<pk>/delete/', views.product_food_delete_view, name="products_food_delete"),
        ])),
        path('drinks/', include([
            path('', views.ProductsDrinkListView.as_view(), name="products_drink_list"),
            path('save/', views.ProductsDrinkCreateView.as_view(), name="products_drink_create"),
            path('<pk>/details/', views.ProductsDrinkDetailsView.as_view(), name="products_drink_details"),
            path('<pk>/delete/', views.product_drink_delete_view, name="products_drink_delete"),
        ])),
    ])),

    path('orders/', include([
        path('', views.OrdersListView.as_view(), name="order_list")
    ])),

]
