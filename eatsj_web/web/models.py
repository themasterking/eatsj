from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager, Group
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

# Create your models here.eatsj
COMPANY_NAME = "ISJ"
DEFAULT_USER = "SYSTEM"

STATE_ORDER_CHOICES = (
    ('sm', "petite faim"),
    ('md', "standard"), 
    ('lg', "grande faim")
)

GENDER_CHOICES = (
    ('M', 'Homme'),
    ('F', 'Femme')
)

DEFAULT_PRICE = {
    'md': 500,
    'sm': 300,
    'lg': 1000
}


class Security(models.Model):
    created_by = models.CharField(_('created by'), max_length=50, default=DEFAULT_USER, blank=True)
    modified_by = models.CharField(_('modified by'), max_length=50, default=DEFAULT_USER, blank=True)
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    modified_at = models.DateTimeField(_('modified at'), auto_now=True)
    is_delete = models.BooleanField(_('delete'), default=False, blank=True)
    deleted_at = models.DateTimeField(_('delete_at'), blank=True, null=True)

    class Meta:
        abstract = True

    def surveillance(self, current_user=None, description=""):
        history = History()
        history.action = 'U'
        history.description = description
        history.instance = self.__class__.__name__

        if current_user:
            history.username = current_user
        else:
            history.username = "system"
            current_user = DEFAULT_USER

        try:
            if self.__class__.objects.get(pk=self.pk):
                history.action = 'U'
        except self.__class__.DoesNotExist:
            history.action = 'C'
            self.created_by = str(current_user)
        except Exception as e:
            history.done = False
            history.description = e
        if history.done:
            self.modified_by = str(current_user)
        return history

    def save(self, email=None, description=None, *args, **kwargs):
        history = self.surveillance(email, description)
        history.save()
        super(Security, self).save(*args, **kwargs)
        return self

    def remove(self, email=None, description=None):
        history = self.surveillance(email, description)
        history.action = 'D'
        history.save()
        self.is_delete = True
        self.deleted_at = timezone.now()
        super(Security, self).save()
        return self

    def delete(self, *args, **kwargs):
        history = self.surveillance()
        history.action = 'P'
        history.save()
        return super(Security, self).delete(*args, **kwargs)


class UserManager(BaseUserManager):
    def create_user(self, username, email, password=None):
        if not username:
            raise ValueError("L'utilisateur doit avoir un nom d'utilisateur")
        if not email:
            raise ValueError("L'utilisateur doit avoir un email")
        user = self.model(username=username, email=self.normalize_email(email))
        user.set_password(password)
        user.is_active = True
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password):
        user = self.create_user(username, email, password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Product(Security):
    image = models.ImageField(upload_to="images/products")
    name = models.CharField("nom", max_length=50)
    price_init = models.FloatField("prix initial (F CFA)", default=DEFAULT_PRICE['md'])
    price_sm_init = models.FloatField("prix petite faim initial (F CFA)", default=DEFAULT_PRICE['sm'])
    price_lg_init = models.FloatField("prix grande faim initial (F CFA)", default=DEFAULT_PRICE['lg'])

    class Meta:
        abstract = True


class Drink(Product):
    nbr_liter = models.FloatField("nombre de litre", default=1)
    menus = models.ManyToManyField("Menu", through="DrinkMenu")

    class Meta:
        verbose_name = "boisson"


class Food(Product):
    description = models.TextField("description", null=True, blank=True)
    menus = models.ManyToManyField("Menu", through="FoodMenu")

    class Meta:
        verbose_name = "plat"


class Menu(Security):
    drinks = models.ManyToManyField(Drink, through="DrinkMenu")
    foods = models.ManyToManyField(Food, through="FoodMenu")

    date_sell = models.DateField("date de vente")

    class Meta:
        verbose_name = "menu"


class ProductMenu(Security):
    qte_init = models.IntegerField("quantité initiale")
    price_sell = models.FloatField("prix de vente", default=DEFAULT_PRICE['md'])

    class Meta:
        abstract = True


class DrinkMenu(ProductMenu):
    drink = models.ForeignKey(Drink, on_delete=models.CASCADE, verbose_name="boisson")
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE, verbose_name="menu")

    class Meta:
        verbose_name = "boisson_au_menu"


class FoodMenu(ProductMenu):
    food = models.ForeignKey(Food, on_delete=models.CASCADE, verbose_name="plat")
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE, verbose_name="menu")

    price_sm_sell = models.FloatField("prix de vente", default=DEFAULT_PRICE['sm'])
    price_lg_sell = models.FloatField("prix de vente", default=DEFAULT_PRICE['lg'])

    class Meta:
        verbose_name = "plat_au_menu"


# class Formule(Security):
#     foodMenus = models.ManyToManyField("FoodMenu", through="FoodMenuFormule") 

#     label = models.CharField("libelle", max_length=50)
#     ratio = models.FloatField("ratio prix")

#     class Meta:
#         verbose_name = "formule"


# class FoodMenuFormule(Security):
#     food_menu = models.ForeignKey(FoodMenu, on_delete=models.CASCADE, verbose_name="plat au menu")
#     formule = models.ForeignKey(Formule, on_delete=models.CASCADE, verbose_name="formule")
#     order_users = models.ManyToManyField("User", through="Order", related_name="commandes_utilisateurs")

#     price_final = models.FloatField("prix final")

#     class Meta:
#         verbose_name = "formule_de_plat_au_menu"


class User(AbstractUser, Security):
    first_name = models.CharField("prenom", max_length=100, blank=True)
    last_name = models.CharField("nom", max_length=100, blank=True)
    gender = models.CharField("sexe", choices=GENDER_CHOICES, max_length=1)
    register = models.CharField("matricule", max_length=100)
    phone = models.CharField("telephone", max_length=100)
    groups = None

    favorite_foods = models.ManyToManyField(Food, related_name="plats_favoris") 
    order_foods = models.ManyToManyField(FoodMenu, through="Order", related_name="plats_commandes")

    objects = UserManager()

    class Meta:
        verbose_name = "utilisateur"


class Order(Security):
    label = models.CharField("label", max_length=50)
    note = models.FloatField("note")
    state = models.CharField("etat", choices=STATE_ORDER_CHOICES, max_length=2)

    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="utilisateur")
    food = models.ForeignKey(FoodMenu, on_delete=models.CASCADE, verbose_name="plat_au_menu")
    drink = models.ForeignKey(DrinkMenu, on_delete=models.CASCADE, verbose_name="boisson_au_menu", blank=True, null=True)

    class Meta:
        verbose_name = "commande"


class Comment(Security):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="utilisateur")
    food_menu = models.ForeignKey(FoodMenu, on_delete=models.CASCADE, verbose_name="plat_au_menu")

    message = models.TextField("message")

    class Meta:
        verbose_name = "commentaires"


class History(models.Model):
    ACTIONS = (
        ('C', _("add")),
        ('U', _("update")),
        ('D', _("delete")),
        ('P', _("purge")),
    )
    username = models.CharField(_('username user'), default="system", max_length=255)
    action = models.CharField(choices=ACTIONS, max_length=100, null=True, verbose_name=_('action'))
    description = models.TextField(null=True, blank=True, verbose_name=_('description'))
    instance = models.CharField(max_length=100, default=DEFAULT_USER, verbose_name=_('instance'))
    when = models.DateTimeField(auto_now_add=True, blank=True)
    done = models.BooleanField(default=True, verbose_name=_('done'))

    class Meta:
        verbose_name = _('history')
        verbose_name_plural = _('histories')

    def __str__(self):
        return f"{_('History N°')}{self.pk}"
