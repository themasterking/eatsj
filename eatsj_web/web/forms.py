from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.forms import fields
from . import models


attrs = {
    'class': 'round pl-3',
    'autocomplete': 'off'
}


class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Nom d'utilisateur", max_length=50, widget=forms.TextInput(attrs=attrs))
    password = forms.CharField(label="Mot de passe", max_length=50, widget=forms.PasswordInput(attrs=attrs))

    def confirm_login_allowed(self, user: models.User) -> None:
        if not (user.is_active and user.is_staff):
            raise forms.ValidationError("Ce compte n'est pas autorisé.")


class UserCreateForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(), label="Mot de passe")
    confirm_password = forms.CharField(widget=forms.PasswordInput(), label="Confirmer le mot de passe")

    class Meta:
        model = models.User
        fields = [
            'last_name', 
            'first_name',
            'gender', 
            'register', 
            'email', 
            'phone', 
            'username', 
            'is_superuser',
            'password',
        ]
    
    def clean(self):
        cleaned_data = super(UserCreateForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")

        if password != confirm_password:
            self.add_error('confirm_password', "Mot de passe incorrect")

        return cleaned_data


class FoodForm(forms.ModelForm):
    class Meta:
        model = models.Food
        fields = [
            'image',
            'name',
            'price_init',
            'description'
        ]
